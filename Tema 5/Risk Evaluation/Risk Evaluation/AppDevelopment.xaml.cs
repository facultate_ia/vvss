﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Risk_Evaluation
{
    /// <summary>
    /// Interaction logic for AppDevelopment.xaml
    /// </summary>
    public partial class AppDevelopment : Window
    {
        public AppDevelopment()
        {
            InitializeComponent();
        }

        private void int1_1_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_1.Content = int.Parse(dev_pondere_1.Content.ToString()) * 1;
        }
        private void int1_2_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_1.Content = int.Parse(dev_pondere_1.Content.ToString()) * 2;
        }
        private void int1_3_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_1.Content = int.Parse(dev_pondere_1.Content.ToString()) * 3;
        }
        private void int1_4_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_1.Content = int.Parse(dev_pondere_1.Content.ToString()) * 4;
        }
        private void int1_5_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_1.Content = int.Parse(dev_pondere_1.Content.ToString()) * 5;
        }
        private void int2_1_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_2.Content = int.Parse(dev_pondere_2.Content.ToString()) * 1;
        }
        private void int2_2_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_2.Content = int.Parse(dev_pondere_2.Content.ToString()) * 2;
        }
        private void int2_3_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_2.Content = int.Parse(dev_pondere_2.Content.ToString()) * 3;
        }
        private void int2_4_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_2.Content = int.Parse(dev_pondere_2.Content.ToString()) * 4;
        }
        private void int2_5_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_2.Content = int.Parse(dev_pondere_2.Content.ToString()) * 5;
        }
        private void int3_1_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_3.Content = int.Parse(dev_pondere_3.Content.ToString()) * 1;
        }
        private void int3_2_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_3.Content = int.Parse(dev_pondere_3.Content.ToString()) * 2;
        }
        private void int3_3_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_3.Content = int.Parse(dev_pondere_3.Content.ToString()) * 3;
        }
        private void int3_4_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_3.Content = int.Parse(dev_pondere_3.Content.ToString()) * 4;
        }
        private void int3_5_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_3.Content = int.Parse(dev_pondere_3.Content.ToString()) * 5;
        }
        private void int4_1_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_4.Content = int.Parse(dev_pondere_4.Content.ToString()) * 1;
        }
        private void int4_2_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_4.Content = int.Parse(dev_pondere_4.Content.ToString()) * 2;
        }
        private void int4_3_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_4.Content = int.Parse(dev_pondere_4.Content.ToString()) * 3;
        }
        private void int4_4_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_4.Content = int.Parse(dev_pondere_4.Content.ToString()) * 4;
        }
        private void int4_5_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_4.Content = int.Parse(dev_pondere_4.Content.ToString()) * 5;
        }
        private void int5_1_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_5.Content = int.Parse(dev_pondere_5.Content.ToString()) * 1;
        }
        private void int5_2_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_5.Content = int.Parse(dev_pondere_5.Content.ToString()) * 2;
        }
        private void int5_3_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_5.Content = int.Parse(dev_pondere_5.Content.ToString()) * 3;
        }
        private void int5_4_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_5.Content = int.Parse(dev_pondere_5.Content.ToString()) * 4;
        }
        private void int5_5_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_5.Content = int.Parse(dev_pondere_5.Content.ToString()) * 5;
        }
        private void int6_1_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_6.Content = int.Parse(dev_pondere_6.Content.ToString()) * 1;
        }
        private void int6_2_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_6.Content = int.Parse(dev_pondere_6.Content.ToString()) * 2;
        }
        private void int6_3_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_6.Content = int.Parse(dev_pondere_6.Content.ToString()) * 3;
        }
        private void int6_4_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_6.Content = int.Parse(dev_pondere_6.Content.ToString()) * 4;
        }
        private void int6_5_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_6.Content = int.Parse(dev_pondere_6.Content.ToString()) * 5;
        }
        private void int7_1_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_7.Content = int.Parse(dev_pondere_7.Content.ToString()) * 1;
        }
        private void int7_2_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_7.Content = int.Parse(dev_pondere_7.Content.ToString()) * 2;
        }
        private void int7_3_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_7.Content = int.Parse(dev_pondere_7.Content.ToString()) * 3;
        }
        private void int7_4_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_7.Content = int.Parse(dev_pondere_7.Content.ToString()) * 4;
        }
        private void int7_5_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_7.Content = int.Parse(dev_pondere_7.Content.ToString()) * 5;
        }
        private void int8_1_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_8.Content = int.Parse(dev_pondere_8.Content.ToString()) * 1;
        }
        private void int8_2_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_8.Content = int.Parse(dev_pondere_8.Content.ToString()) * 2;
        }
        private void int8_3_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_8.Content = int.Parse(dev_pondere_8.Content.ToString()) * 3;
        }
        private void int8_4_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_8.Content = int.Parse(dev_pondere_8.Content.ToString()) * 4;
        }
        private void int8_5_Click(object sender, RoutedEventArgs e)
        {
            dev_punctaj_8.Content = int.Parse(dev_pondere_8.Content.ToString()) * 5;
        }

        private void dev_punctajTotal_Click(object sender, RoutedEventArgs e)
        {
            if (dev_punctaj_1.Content.ToString() != "" &&
                dev_punctaj_2.Content.ToString() != "" &&
                dev_punctaj_3.Content.ToString() != "" &&
                dev_punctaj_4.Content.ToString() != "" &&
                dev_punctaj_5.Content.ToString() != "" &&
                dev_punctaj_6.Content.ToString() != "" &&
                dev_punctaj_7.Content.ToString() != "" &&
                dev_punctaj_8.Content.ToString() != "")
            {
                dev_punctaj_total.Content = int.Parse(dev_punctaj_1.Content.ToString()) +
                    int.Parse(dev_punctaj_2.Content.ToString()) +
                    int.Parse(dev_punctaj_3.Content.ToString()) +
                    int.Parse(dev_punctaj_4.Content.ToString()) +
                    int.Parse(dev_punctaj_5.Content.ToString()) +
                    int.Parse(dev_punctaj_6.Content.ToString()) +
                    int.Parse(dev_punctaj_7.Content.ToString()) +
                    int.Parse(dev_punctaj_8.Content.ToString());
                dev_trimiteEvaluare.IsEnabled = true;
            }
            else
            {
                MessageBox.Show("Vă rugăm răspundeţi la toate întrebările!", "Au rămas întrebări nerăspunse",
                    MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void dev_trimiteEvaluare_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Formularul a fost trimis cu succes!", "Formular trimis",
                MessageBoxButton.OK, MessageBoxImage.Information);
            this.Close();
        }
    }
}
