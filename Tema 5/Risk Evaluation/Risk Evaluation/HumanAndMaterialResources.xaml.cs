﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Risk_Evaluation
{
    /// <summary>
    /// Interaction logic for HumanAndMaterialResources.xaml
    /// </summary>
    public partial class HumanAndMaterialResources : Window
    {
        public HumanAndMaterialResources()
        {
            InitializeComponent();
        }

        private void int1_1_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_1.Content = int.Parse(resources_pondere_1.Content.ToString()) * 1;
        }
        private void int1_2_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_1.Content = int.Parse(resources_pondere_1.Content.ToString()) * 2;
        }
        private void int1_3_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_1.Content = int.Parse(resources_pondere_1.Content.ToString()) * 3;
        }
        private void int1_4_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_1.Content = int.Parse(resources_pondere_1.Content.ToString()) * 4;
        }
        private void int1_5_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_1.Content = int.Parse(resources_pondere_1.Content.ToString()) * 5;
        }
        private void int2_1_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_2.Content = int.Parse(resources_pondere_2.Content.ToString()) * 1;
        }
        private void int2_2_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_2.Content = int.Parse(resources_pondere_2.Content.ToString()) * 2;
        }
        private void int2_3_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_2.Content = int.Parse(resources_pondere_2.Content.ToString()) * 3;
        }
        private void int2_4_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_2.Content = int.Parse(resources_pondere_2.Content.ToString()) * 4;
        }
        private void int2_5_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_2.Content = int.Parse(resources_pondere_2.Content.ToString()) * 5;
        }
        private void int3_1_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_3.Content = int.Parse(resources_pondere_3.Content.ToString()) * 1;
        }
        private void int3_2_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_3.Content = int.Parse(resources_pondere_3.Content.ToString()) * 2;
        }
        private void int3_3_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_3.Content = int.Parse(resources_pondere_3.Content.ToString()) * 3;
        }
        private void int3_4_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_3.Content = int.Parse(resources_pondere_3.Content.ToString()) * 4;
        }
        private void int3_5_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_3.Content = int.Parse(resources_pondere_3.Content.ToString()) * 5;
        }
        private void int4_1_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_4.Content = int.Parse(resources_pondere_4.Content.ToString()) * 1;
        }
        private void int4_2_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_4.Content = int.Parse(resources_pondere_4.Content.ToString()) * 2;
        }
        private void int4_3_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_4.Content = int.Parse(resources_pondere_4.Content.ToString()) * 3;
        }
        private void int4_4_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_4.Content = int.Parse(resources_pondere_4.Content.ToString()) * 4;
        }
        private void int4_5_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_4.Content = int.Parse(resources_pondere_4.Content.ToString()) * 5;
        }
        private void int5_1_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_5.Content = int.Parse(resources_pondere_5.Content.ToString()) * 1;
        }
        private void int5_2_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_5.Content = int.Parse(resources_pondere_5.Content.ToString()) * 2;
        }
        private void int5_3_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_5.Content = int.Parse(resources_pondere_5.Content.ToString()) * 3;
        }
        private void int5_4_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_5.Content = int.Parse(resources_pondere_5.Content.ToString()) * 4;
        }
        private void int5_5_Click(object sender, RoutedEventArgs e)
        {
            resources_punctaj_5.Content = int.Parse(resources_pondere_5.Content.ToString()) * 5;
        }

        private void resources_punctajTotal_Click(object sender, RoutedEventArgs e)
        {
            if (resources_punctaj_1.Content.ToString() != "" &&
                resources_punctaj_2.Content.ToString() != "" &&
                resources_punctaj_3.Content.ToString() != "" &&
                resources_punctaj_4.Content.ToString() != "" &&
                resources_punctaj_5.Content.ToString() != "")
            {
                resources_punctaj_total.Content = int.Parse(resources_punctaj_1.Content.ToString()) +
                    int.Parse(resources_punctaj_2.Content.ToString()) +
                    int.Parse(resources_punctaj_3.Content.ToString()) +
                    int.Parse(resources_punctaj_4.Content.ToString()) +
                    int.Parse(resources_punctaj_5.Content.ToString());
                resources_trimiteEvaluare.IsEnabled = true;
            }
            else
            {
                MessageBox.Show("Vă rugăm răspundeţi la toate întrebările!", "Au rămas întrebări nerăspunse",
                    MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void resources_trimiteEvaluare_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Formularul a fost trimis cu succes!", "Formular trimis",
                MessageBoxButton.OK, MessageBoxImage.Information);
            this.Close();
        }
    }
}
