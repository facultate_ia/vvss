﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Risk_Evaluation
{
    /// <summary>
    /// Interaction logic for RiskEvaluation.xaml
    /// </summary>
    public partial class RiskEvaluation : Window
    {
        public RiskEvaluation()
        {
            InitializeComponent();
        }

        private void DataOperationButton_Click(object sender, RoutedEventArgs e)
        {
            DataOperation dataOperation = new DataOperation();
            dataOperation.ShowDialog();
        }

        private void AppCreationButton_Click(object sender, RoutedEventArgs e)
        {
            AppCreation appCreation = new AppCreation();
            appCreation.ShowDialog();
        }

        private void AppDevelopmentButton_Click(object sender, RoutedEventArgs e)
        {
            AppDevelopment appDevelopment = new AppDevelopment();
            appDevelopment.ShowDialog();
        }

        private void HumanAndMaterialResourcesButton_Click(object sender, RoutedEventArgs e)
        {
            HumanAndMaterialResources humanAndMaterialResources = new HumanAndMaterialResources();
            humanAndMaterialResources.ShowDialog();
        }

        private void SoftAcquisitionButton_Click(object sender, RoutedEventArgs e)
        {
            SoftAcquisition softAcquisition = new SoftAcquisition();
            softAcquisition.ShowDialog();
        }

        private void OtherFunctionsButton_Click(object sender, RoutedEventArgs e)
        {
            OtherFunctions otherFunctions = new OtherFunctions();
            otherFunctions.ShowDialog();
        }
    }
}
